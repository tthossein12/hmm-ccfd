import config as cfg
import os
import csv


def is_csv(filename):
    if '.csv' in filename:
        return True
    else:
        return False


def main_function():
    print 'splitting e-commerce/face-to-face ...'
    pathtoraws = cfg.raws
    pathtof2f = cfg.f2f
    pathtoecom = cfg.ecom

    ls_name = os.listdir(pathtoraws)
    ls_name.sort()

    for filename in ls_name:
        if is_csv(filename):
            print filename
            origin = open(pathtoraws + filename, 'r')
            origin_reader = csv.reader(origin, delimiter=',', quotechar='"')
            header = next(origin)

            f2f_tx = open(pathtof2f + 'F2F' + filename, 'w')
            f2f_tx.write(header)
            f2f_writer = csv.writer(f2f_tx, delimiter=',', quotechar='"',
                                    quoting=csv.QUOTE_MINIMAL)
            ecom_tx = open(pathtoecom + 'ECOM' + filename, 'w')
            ecom_tx.write(header)
            ecom_writer = csv.writer(ecom_tx, delimiter=',', quotechar='"',
                                     quoting=csv.QUOTE_MINIMAL)

            for row in origin_reader:
                # On checke la valeur de la variable 'TX_ECOM'
                if row[8] == 'TRUE':
                    ecom_writer.writerow(row)
                elif row[8] == 'FALSE':
                    f2f_writer.writerow(row)

            origin.close()
            f2f_tx.close()
            ecom_tx.close()


if __name__ == '__main__':
    main_function()
