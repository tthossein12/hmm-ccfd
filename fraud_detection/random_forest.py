import pandas as pd
from preprocessing import preprocessing as prep
from sklearn.ensemble import RandomForestClassifier
import cPickle
from sklearn import metrics
import numpy as np
import scipy as sp
from scipy import *
import config as cfg
import warnings
import random as rd


# @profile
def train_and_test_rf(df_rf, df_test_rf, df_val_rf):
    """
    Train the new and control random forest models
     Test the efficiency of the models
     Plot the figures for the results
    """
    cfg.log('START RANDOM FOREST \n')
    df_val_rf=df_val_rf.sample(frac=0.2)

    df_val_rf.reset_index(drop=True, inplace=True)
    df_test_rf.reset_index(drop=True, inplace=True)

    #    df_rf = df_rf[df_rf['TX_DATETIME'] < np.datetime64(cfg.t_val)]
    print df_rf['TX_FRAUD'][:25]

    pd.DataFrame.very_deep_copy = cfg.very_deep_copy
    
    feat_rf = ['TX_DATETIME','TX_EMV','TERM_MCC', 'TERM_COUNTRY', 'TX_AMOUNT', 'TX_3D_SECURE','TX_ECOM_IND', 'DT_wday',
               'DT_hour', 'TX_INTL',
               'AGE', 'GENDER', 'BROKER', 'CARD_TYPE', 'CARD_AUTHENTICATION',
               'TX_CARD_ENTRY_MODE']

    feat_rf += ['tdelta']
    train_random_forest(df_rf, df_val_rf, 'rf_ctrl', feat_rf, feat_to_add=[])
    test_random_forest(df_test_rf.very_deep_copy(), 'control', [], 'rf_ctrl', feat_rf)


    train_random_forest(df_rf, df_val_rf, 'rf_all_3_5_noagg', feat_rf,
                        feat_to_add=['3_5_amount_CH_fraud',
                                     '3_5_amount_CH_genuine',
                                     '3_5_amount_TM_fraud',
                                     '3_5_amount_TM_genuine',
                                     '3_5_tdelta_CH_fraud',
                                     '3_5_tdelta_CH_genuine',
                                     '3_5_tdelta_TM_fraud',
                                     '3_5_tdelta_TM_genuine'])
    test_random_forest(df_test_rf.very_deep_copy(),
                       'all_features_noagg',
                       ['3_5_amount_CH_fraud', '3_5_amount_CH_genuine',
                        '3_5_amount_TM_fraud', '3_5_amount_TM_genuine',
                        '3_5_tdelta_CH_fraud', '3_5_tdelta_CH_genuine',
                        '3_5_tdelta_TM_fraud', '3_5_tdelta_TM_genuine'],
                       'rf_all_3_5_noagg', feat_rf)

    feat_rf += ['aggCH1', 'aggCH2', 'aggCH3', 'aggCH4']

    train_random_forest(df_rf, df_val_rf, 'rf_ctrlaggCH', feat_rf, feat_to_add=[])
    test_random_forest(df_test_rf.very_deep_copy(), 'controlaggCH', [], 'rf_ctrlaggCH', feat_rf)

    train_random_forest(df_rf, df_val_rf, 'rf_all_3_5CH', feat_rf,
                        feat_to_add=['3_5_amount_CH_fraud',
                                     '3_5_amount_CH_genuine',
                                     '3_5_amount_TM_fraud',
                                     '3_5_amount_TM_genuine',
                                     '3_5_tdelta_CH_fraud',
                                     '3_5_tdelta_CH_genuine',
                                     '3_5_tdelta_TM_fraud',
                                     '3_5_tdelta_TM_genuine'])
    test_random_forest(df_test_rf.very_deep_copy(),
                       'all_featuresCH',
                       ['3_5_amount_CH_fraud', '3_5_amount_CH_genuine',
                        '3_5_amount_TM_fraud', '3_5_amount_TM_genuine',
                        '3_5_tdelta_CH_fraud', '3_5_tdelta_CH_genuine',
                        '3_5_tdelta_TM_fraud', '3_5_tdelta_TM_genuine'],
                       'rf_all_3_5CH', feat_rf)


    feat_rf += ['aggTM1', 'aggTM2', 'aggTM3', 'aggTM4']

    train_random_forest(df_rf, df_val_rf, 'rf_ctrlagg', feat_rf, feat_to_add=[])
    test_random_forest(df_test_rf.very_deep_copy(), 'controlagg', [], 'rf_ctrlagg', feat_rf)

    train_random_forest(df_rf, df_val_rf, 'rf_all_3_5', feat_rf,
                        feat_to_add=['3_5_amount_CH_fraud',
                                     '3_5_amount_CH_genuine',
                                     '3_5_amount_TM_fraud',
                                     '3_5_amount_TM_genuine',
                                     '3_5_tdelta_CH_fraud',
                                     '3_5_tdelta_CH_genuine',
                                     '3_5_tdelta_TM_fraud',
                                     '3_5_tdelta_TM_genuine'])
    test_random_forest(df_test_rf.very_deep_copy(),
                       'all_features',
                       ['3_5_amount_CH_fraud', '3_5_amount_CH_genuine',
                        '3_5_amount_TM_fraud', '3_5_amount_TM_genuine',
                        '3_5_tdelta_CH_fraud', '3_5_tdelta_CH_genuine',
                        '3_5_tdelta_TM_fraud', '3_5_tdelta_TM_genuine'],
                       'rf_all_3_5', feat_rf)



def train_random_forest(df_rf, df_val_rf, pathtosave, feat_rf, feat_to_add=[]):
    """
    Train the random forest model
    """
    warnings.filterwarnings("ignore", category=DeprecationWarning)

    cfg.log(str(df_rf.columns) + '\n')
    x_val, y_val = prep.preprocessing_from_pandas_to_sklearn(df_val_rf.copy(),

                                                             feat_rf + feat_to_add)
    x_train, y_train = prep.preprocessing_from_pandas_to_sklearn(df_rf.copy(),
                                                                 feat_rf + feat_to_add)
    cfg.log(str(np.shape(x_val)) + '\n')
    param_grid = {"max_features": [5, 10, 1],
                  "min_samples_leaf": [1, 10, 30],
                  "criterion": ['gini', 'entropy']}

    model = rdsearch(x_train, y_train, x_val, y_val, param_grid, pr,
                     pathtosave)

    cfg.log(str(model.feature_importances_) + '\n')


def pr_auc_02(y, pred):
    p, r, t = metrics.precision_recall_curve(y, pred)
    return getAUC(r, p, upto=0.2)


def roc_auc(y, pred):
    return metrics.roc_auc_score(y, pred)


def pr(y, pred):
    p, r, t = metrics.precision_recall_curve(y, pred)
    return metrics.auc(r, p)


def rdsearch(x_train, y_train, x_val, y_val, param_grid, scorer, pathtosave):
    best_score = 0
    best_clf = 0
    print x_train.head()
    x_train.drop(['TX_DATETIME'], axis=1, inplace=True)
    x_val.drop(['TX_DATETIME'], axis=1, inplace=True)
    for max_features in param_grid['max_features']:
        for min_samples_leaf in param_grid['min_samples_leaf']:
            for criterion in param_grid['criterion']:
                clf = RandomForestClassifier(n_estimators=600,
                                             min_samples_leaf=min_samples_leaf, max_features=max_features,
                                             criterion=criterion,
                                             n_jobs=-1)
                print x_val.head()
                clf.fit(x_train, y_train)

                pred = clf.predict_proba(x_val)
                score = scorer(y_val, pred[:, 1])
                if score > best_score:
                    print score
                    best_clf = clf
                    best_score = score
    with open(cfg.results + 'model_' + pathtosave, 'w') as saver:
        cPickle.dump(best_clf, saver)
    return best_clf


def test_random_forest(df, extension, feat_to_add, model_path, feat_rf):
    with open(cfg.results + 'model_' + model_path, 'r') as loader:
        detection_module = cPickle.load(loader)

    results = []
    scores = []
    test_day = np.datetime64(cfg.t_test)
    while test_day < np.datetime64('2015-05-31 23:59:59'):
        df_test = df[df['TX_DATETIME'] > np.datetime64(test_day)]
        df_test = df_test[
            df_test['TX_DATETIME'] < np.datetime64(test_day + 24 * 3600)]
        test_day += 24 * 3600  # seconds

        d_test, y_test = prep.preprocessing_from_pandas_to_sklearn(
            df_test.copy(), feat_rf + feat_to_add)
        d_test.drop(['TX_DATETIME'], axis=1, inplace=True)
        pred_test = detection_module.predict_proba(d_test)
        print np.shape(pred_test)
        results.append([y_test, pred_test[:, 1]])
        scores.append(pr(y_test, pred_test[:, 1]))
    cfg.log(str(np.mean(scores)))
    print np.shape(df)
    with open(cfg.results + extension + '.pkl',
              'w') as save:
        cPickle.dump(results, save)


def scores_in_log(keyword):
    with open(cfg.results + keyword + '.pkl',
              'r') as load_res:
        [y, pred] = cPickle.load(load_res)

    p, r, t = metrics.precision_recall_curve(y, pred[:, 1])

    cfg.log(': AUC = ' + str(getAUC(r, p)) + ' for keyword : ' + keyword + '\n')
    cfg.log(': AUC 0.2 = ' + str(
        getAUC(r, p, upto=0.2)) + ' for keyword : ' + keyword + '\n')


